'use strict';

angular.module('weatherStation')
    .factory('WeatherModel', [function() {

        function Weather(data) {
            if (!data.weather) {
                data.weather = [{
                    main: '',
                    icon: ''
                }];
            }
            if (!data.main) {
                data.main = {};
            }
            this.city = data.name || '';
            this.location = data.coord || {
                lat: '',
                lon: ''
            };
            this.currentCondition = data.weather[0].main || '';
            this.icon = data.weather[0].icon || '';
            this.temp = data.main.temp || '';
            this.tempMax = data.main.temp_max || '';
            this.tempMin = data.main.temp_min || '';
            this.pressure = data.main.pressure || '';
            this.humidity = data.main.humidity || '';
        }

        return Weather;
    }]);