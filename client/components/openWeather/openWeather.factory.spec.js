'use strict';

describe('openWeather Factory', function() {
    var openWeather, $httpBackend, mockResponse;

    beforeEach(module('weatherStation', 'mockData'));

    it('should be defined', function() {

        inject(function(_openWeather_, _$httpBackend_, _mockResponse_) {
            openWeather = _openWeather_;
            $httpBackend = _$httpBackend_;
            mockResponse = _mockResponse_;
        })

        expect(openWeather).toBeDefined();
    });

    describe('openWeather.all', function() {

        it('should return data with the requested properties', function() {
            var ids = [
                '2643743',
                '2643339'
            ];

            $httpBackend
                .expectGET('http://api.openweathermap.org/data/2.5/group?id=' + ids + '&units=metric')
                .respond(200, mockResponse);

            openWeather
                .all(ids)
                .then(function(data) {
                    expect(data.length).toEqual(2);
				});
            $httpBackend.flush();
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();

        });
    });
});