'use strict';

describe('Weather Model', function() {
    var WeatherModel, mockResponse;

    beforeEach(module('weatherStation', 'mockData'));

    it('should be defined', function() {

        inject(function(_WeatherModel_, _mockResponse_) {
            WeatherModel = _WeatherModel_;
            mockResponse = _mockResponse_;
        })

        expect(WeatherModel).toBeDefined();
    });

    describe('new WeatherModel(data)', function() {

        it('should return a object with value based on the input data', function() {
            var weather = new WeatherModel(mockResponse.list[0]);
            expect(weather).toEqual({
                city: 'Moscow',
                location: {
                    lon: 37.62,
                    lat: 55.75
                },
                currentCondition: 'Clouds',
                icon: '04n',
                temp: 19.39,
                tempMax: 19.555,
                tempMin: 19.222,
                pressure: 998.76,
                humidity: 35
            });
        });
        it('should return a object with default value if the input data is an empty obj', function() {
            var weather = new WeatherModel(mockResponse.list[1]);
            expect(weather).toEqual({
                city: '',
                location: {
                    lat: '',
                    lon: ''
                },
                currentCondition: '',
                icon: '',
                temp: '',
                tempMax: '',
                tempMin: '',
                pressure: '',
                humidity: ''
            });
        });
    });
});