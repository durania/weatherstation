'use strict';

angular.module('weatherStation')
    .factory('openWeather', ['$q', '$http','WeatherModel', function($q, $http, WeatherModel) {

        return {
            all: function(cities) {
                var deffered = $q.defer();
                $http.get('http://api.openweathermap.org/data/2.5/group?id=' + cities.join(',') + '&units=metric')
                    .success(function(data) {
                        var res = [];
                        data.list.forEach(function(d) {
                            if (d) {
                                var item = new WeatherModel(d);
                                res.push(item);
                            }
                        });
                        deffered.resolve(res);
                    })
                    .error(function() {

                        deffered.reject({
                            msg: "Something Very Bad happened to the Open Wheather server"
                        });
                    });

                return deffered.promise;

            }
        };
    }]);