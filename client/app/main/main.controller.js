'use strict';

angular.module('weatherStation')
    .value('cities', [
        '2643743',
        '2643339',
        '2643123',
        '2655603'
    ])
    .controller('MainCtrl', ['$scope', 'openWeather', 'cities', function($scope, openWeather, cities) {
        $scope.error = null;
        $scope.isLoading = true;
        $scope.list = [];
        $scope.selected = null;
        
        openWeather.all(cities)
            .then(function(data) {
                $scope.list = data;
            })
            .catch(function(err) {
                $scope.error = err;
            })
            .finally(function() {
                $scope.isLoading = false;
            });

        $scope.showDetails = function(selected) {
            $scope.selected = selected;
            $scope.list.forEach(function(item) {
                item.isActive = false;
                if (item.city === selected.city) {
                    item.isActive = true;
                };
            });
        };
    }]);